/**
 * `ejercicio-ranas`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class EjercicioRanas extends Polymer.Element {
  
  static get is() { return 'ejercicio-ranas'; }
  
  static get properties() {
    return {
      arrayPosicion: {
        type: Array,
        value: [
                {position: "X"},
                {position: ""},
                {position: ""},
                {position: ""},
                {position: ""},
                {position: ""},
                {position: ""},
                {position: ""}
              ]
      }
    };
  }

  _saltar(){
    let saltos = this.$.txtSaltos.value;
    
    saltos = parseInt(saltos);

    if(isNaN(saltos))
      saltos = 0;

    //Where is the frog?
    let index = 0;
    for(var i = 0; i < this.arrayPosicion.length; i++){
      let valuePosition = this.arrayPosicion[i].position;
      if(valuePosition === "X") index = i;
    }
    //Where will it jump?
    let limite = this.arrayPosicion.length - 1;

    let newPosition = index + saltos;

    if(newPosition > limite){
      do{
        newPosition = newPosition - limite - 1;
      }while(newPosition > limite)
    }

    console.log(newPosition);

    this.set('arrayPosicion.' + index, {position: ""});
    this.set('arrayPosicion.' + newPosition, {position: "X"} );
  }

}

window.customElements.define(EjercicioRanas.is, EjercicioRanas);
/**
 * `ejercicio-agenda`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class EjercicioAgenda extends Polymer.Element {

  static get is() { return 'ejercicio-agenda'; }

  static get properties() {
    return {

      labelButton: {
        type: String,
        value: 'Agregar'
      },

      labelEvent: {
        type: String,
        value: 'Evento: '
      },

      placeholderInput: {
        type: String,
        value: 'Escribe el nombre de tu evento'
      },

      listaEventos: {
        type: Array,
        value: []
      }

    };
  }

  _addEvent() {
    let inputValue = this.$.txtEventName.value;
    let idEvent = 0;
    if(this.listaEventos.length > 0){
      idEvent = this.listaEventos[this.listaEventos.length - 1].idEvent + 1;
    }
    if(inputValue !== ''){
      this.push('listaEventos', {idEvent: idEvent, eventName: inputValue});
    }
    this.$.txtEventName.value = '';
  }

  _removeEvent(e) {
    let idEvent = e.path[0].getAttribute('id');
    idEvent = parseInt(idEvent);
    let index = 0;

    for(var i = 0; i < this.listaEventos.length; i++){
      let valueIdEvent = this.listaEventos[i].idEvent;
      if(valueIdEvent === idEvent) index = i;
    }

    if(this.listaEventos.length > 0){
      this.splice('listaEventos', index, 1);
    }
  }

}

window.customElements.define(EjercicioAgenda.is, EjercicioAgenda);
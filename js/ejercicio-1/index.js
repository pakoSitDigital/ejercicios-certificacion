"use strict";

var buttons = {

  init: function(){
    this.events.buttons.btnCreateButtons();
  },

  events: {

    buttons: {

      btnCreateButtons: function(){

        var button = document.getElementById('btn-create-buttons');
        button.onclick = buttons.methods.createButtons;

      }

    }

  },

  methods: {

    createButtons: function(){

      //Get qty buttons:
      var qtyButtons = buttons.methods.getQtyButtons();
      if(qtyButtons === ""){
        qtyButtons = 0;
      }else{
        qtyButtons = parseInt(qtyButtons);
        if(isNaN(qtyButtons)) 
          qtyButtons = 0;
      }

      var containerButtons = document.getElementById("container-buttons");
      containerButtons.innerHTML = "";

      //Iterate the qty of buttons to create:
      for(var i = 1; i <= qtyButtons; i++){
        var button = document.createElement("button");
        button.innerHTML = "Botón " + i;
        button.onclick = buttons.methods.buttonAlertIndex;
        button.setAttribute("data-index", (i-1));
        containerButtons.appendChild(button)
      }

    },

    getQtyButtons: function(){

      var inputText = document.getElementById('txt-qty-buttons');
      return inputText.value;

    },

    buttonAlertIndex: function(e){
      var index = this.getAttribute("data-index");
      alert("Button index: " + index);
    }

  }

};

window.onload = function(){
  buttons.init();
}

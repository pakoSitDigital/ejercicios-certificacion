"use strict";

var countArrayItems = {

  array: [1,2,3,5,4,2,["a","b","c"],6,8,5,1,2,{nombre: "pako"},7,2,3,8,7,5,4,3,1,2,3,"a",5,8,3],

  init: function(){
    
    this.methods.paintArray();
    this.methods.countItems();

  },

  events: {

    

  },

  methods: {

    paintArray: function() {

      var containerArray = document.getElementById('container-array');
      var tempArray = [];
      for(var i = 0; i < countArrayItems.array.length; i++){
        var arrayValue = countArrayItems.array[i];
        if(typeof arrayValue === "object"){
          tempArray.push(JSON.stringify(arrayValue))
        }else{
          tempArray.push(arrayValue.toString());
        }
      }
      var arrayText = tempArray.toString();
      containerArray.innerHTML = "[" + arrayText + "]";
      
    },

    countItems: function() {

      var elements = countArrayItems.methods.getDiferentElements(countArrayItems.array);
      var containerResult = document.getElementById("container-results");
      var textHTML = "";

      for(var i = 0; i < elements.length; i++){
        textHTML = textHTML + elements[i] + "<br/>";
      }

      containerResult.innerHTML = textHTML;

    },

    getDiferentElements: function(array) {

      var tempArray = [];
      var tempValue;
      var count = 0;
      
      array = array.sort();

      for(var i = 0; i < array.length; i++){

        var arrayValue = array[i];

        if(typeof arrayValue === "object"){
          arrayValue = JSON.stringify(arrayValue);
        }else{
          arrayValue = arrayValue.toString();
        }

        if(arrayValue !== tempValue){
          if(typeof tempValue !== "undefined"){
            tempArray.push("El elemento con valor '" + tempValue + "' se repite " + count + " veces.");
          }
          if(array.length === (i + 1)){
            tempArray.push("El elemento con valor '" + arrayValue + "' se repite 1 veces.");
          }
          count = 1;
        } else {
          if(array.length === (i + 1)){
            tempArray.push("El elemento con valor '" + arrayValue + "' se repite " + count + " veces.");
          }else{
            count = count + 1;
          }
        }

        tempValue = arrayValue;

      }

      return tempArray;
      
    }

  }

};

window.onload = function(){
  countArrayItems.init();
}
